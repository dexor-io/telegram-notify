#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="dexor/telegram-notify"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}

@test "Smoke test" {
    run docker run \
        -e BOT_TOKEN=$TEST_BOT_TOKEN \
        -e CHAT_ID=$TEST_CHAT_ID \
        -e MESSAGE="_Test passed!_" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

