#!/usr/bin/env bash

source "$(dirname "$0")/common.sh"

# Required parameters
BOT_TOKEN=${BOT_TOKEN:?'BOT_TOKEN variable missing.'}
CHAT_ID=${CHAT_ID:?'CHAT_ID variable missing.'}
MESSAGE="${MESSAGE:?'MESSAGE variable missing.'}"

# Constants
API_URL="https://api.telegram.org/bot${BOT_TOKEN}"
DEFAULT_TITLE="Notification sent from [Pipeline \\#${BITBUCKET_BUILD_NUMBER}](https://bitbucket.org/${BITBUCKET_WORKSPACE}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER})"
DEFAULT_FAIL_MESSAGE="❌ Build failed"

# Default parameters
DEBUG=${DEBUG:="false"}
FORMAT=${FORMAT:="MarkdownV2"}
FAIL_MESSAGE="${FAIL_MESSAGE:="$DEFAULT_FAIL_MESSAGE"}"
TITLE="${TITLE:="$DEFAULT_TITLE"}}"


info "Executing the pipe..."
enable_debug
check_for_newer_version

if [[ ${BITBUCKET_EXIT_CODE} -eq 0 ]]; then
  export build_status=success
else
  export build_status=failed
fi

run curl "${API_URL}/getMe"
if [[ `cat $output_file` = *"\"ok\":true"* ]]; then
  debug "BOT_TOKEN seems to work"
else
  fail "Could not get Telegram bot info. Please check BOT_TOKEN!"
fi

run curl "${API_URL}/getChat?chat_id=${CHAT_ID}"
if [[ `cat $output_file` = *"\"ok\":true"* ]]; then
  debug "CHAT_ID seems to be correct"
else
  fail "Could not get Telegram chat info. Please check CHAT_ID!"
fi

debug "Message text:"
debug "${MESSAGE}"

run curl -s -X POST "${API_URL}/sendMessage?parse_mode=${FORMAT}&chat_id=${CHAT_ID}" -d text="*${TITLE}*%0A%0A${MESSAGE}"

if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error!"
fi
