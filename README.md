# Bitbucket Pipelines Pipe: Telegram Notify

Sends a custom notification to [Telegram](https://telegram.org).

Use this pipe to send your own notifications at any point in your pipelines.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: dexor/telegram-notify:0.1.0
  variables:
    BOT_TOKEN: '<string>'
    CHAT_ID: '<string>'
    MESSAGE: '<string>'
    # TITLE: '<string>' # Optional.
    # FAIL_MESSAGE: '<string>' # Optional.
    # FORMAT: '<string>' # Optional [MarkdownV2 (default), Markdown, HTML].
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable           | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| BOT_TOKEN (*)   | API token for Telegram bot.  |
| CHAT_ID (*)     | ID of the target chat.  |
| MESSAGE (*)     | Notification message text. |
| TITLE           | Notification title. - Default: `Notification sent from <pipeline link>` |
| FAIL_MESSAGE    | Message for failed pipeline when `$BITBUCKET_EXIT_CODE = 1` |
| FORMAT          | Text format in the message. See [Telegram API docs][formatting]. Default: `MarkdownV2` |
| DEBUG           | Turn on extra debug information. Default: `false`. | 

_(*) = required variable._

## Prerequisites

You need to create a Telegram bot, unless you already have one set up. Creating a bot is an automated process and completely free of charge. A detailed explanation how to create a telegram bot can be found here https://core.telegram.org/bots.

In short:

* Contact Telegram's BotFather bot via your Telegram client. Here is a direct link to start chatting with the bot: <https://t.me/botfather>. 
* Send `/start` as message to BotFather. It will respond with a list of all possible commands.
* Send `/newbot` as message to BotFather. It will start an interactive process to create your bot.
* At the end of the process you will receive a long alpha-numeric token to control your new bot. Keep this token save and set it up as **BOT_TOKEN** environment variable or this pipe.

Next step will be to add your new bot to the Telegram channel into which you want it to post messages. Add it just like an other regular user via the search function. You need to give the bot admin access rights to the channel. 

Finally you need to find out the chat-id of your channel. The easiest way is to send a message which will trigger the bot like e.g. `/help` to the chat after you added the bot to it. Now open the URL <https://api.telegram.org/bot{BOT_TOKEN}/getUpdates> in a browser of your choice. You should see the message which the bot received (i.e. the `/start` message). Along with the message you will see the chat-id which is a large negative number starting with "-100". Copy the whole number including the minus prefix. This is your **CHAT_ID** which you also need to setup as an environment variable for this pipe.

## Examples

Basic example:
    
```yaml
script:
  - pipe: dexor/telegram-notify:0.1.0
    variables:
      BOT_TOKEN: $BOT_TOKEN
      CHAT_ID: $CHAT_ID
      MESSAGE: "✅ Build succeeded!"
      FAIL_MESSAGE: "❎ Build failed"
```

Advanced example:

If you want to pass complex string with structure elements, use double quotes

```yaml
script:
  - pipe: dexor/telegram-notify:0.1.0
    variables:
      BOT_TOKEN: $BOT_TOKEN
      CHAT_ID: $CHAT_ID
      FORMAT: HTML
      TITLE: "<b>Build finished!</b>"
      MESSAGE: "[<b>${ENVIRONMENT_NAME}</b>] build has exited with status <i>$build_status</i>"
```

## Support
If you'd like help with this pipe, or you have an issue or feature request.

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2021 Dexor Technology.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.

[Docs]: https://core.telegram.org/bots/api#formatting-options
[formatting]: https://core.telegram.org/bots/api#formatting-options